<?php

namespace app\modules\api\controllers;

use app\models\LoginForm;
use Yii;

class SiteController extends BaseController
{
	public function verbs()
	{
		return [
			'login' => ['POST', 'OPTIONS']
		];
	}

	public function actionLogin()
	{
		$model = new LoginForm();
		$model->load(Yii::$app->request->getBodyParams(), '');

		if ($model->login()) {
			return $model->getUser();
		}

		return $model;
	}
}