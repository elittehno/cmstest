<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->widget(\vova07\imperavi\Widget::className()) ?>

    <?= $form->field($model, 'categoryId')->dropDownList(\app\models\Category::getAll(), ['prompt' => 'Выберите категорию']) ?>

	<?= $form->field($model, 'image')->fileInput() ?>

	<?php if ($model->image): ?>

		<p>
			<?= Html::img('/' . $model->image, ['class' => 'img-responsive']) ?>
		</p>

	<?php endif; ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

{}