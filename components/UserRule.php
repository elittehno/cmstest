<?php

namespace app\components;

use Yii;
use yii\rbac\Rule;
use app\models\User;

/**
 * UserRule проверка прав доступа
 */
class UserRule extends Rule
{
	/**
	 * @inheritdoc
	 */
	public $name = 'role';

	/**
	 * @inheritdoc
	 */
	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			/** @var $user User */
			$user = Yii::$app->user->identity;
			if ($item->name === 'admin') {
				return $user->role === 'admin';
			}
		}

		return false;
	}
}