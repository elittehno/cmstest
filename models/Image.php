<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property int $newsId Статья
 * @property string $filename Изображение
 * @property string $createdAt Дата создания
 * @property string $updatedAt Дата изменения
 *
 * @property Article $news
 */
class Image extends BaseModel
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'images';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['newsId', 'filename'], 'required'],
			[['newsId'], 'integer'],
			[['createdAt', 'updatedAt'], 'safe'],
			[['filename'], 'string', 'max' => 128],
			[['newsId'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['newsId' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'newsId' => 'Статья',
			'filename' => 'Изображение',
			'createdAt' => 'Дата создания',
			'updatedAt' => 'Дата изменения',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getNews()
	{
		return $this->hasOne(Article::className(), ['id' => 'newsId']);
	}
}
