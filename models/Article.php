<?php

namespace app\models;

use app\components\ImageHelper;
use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title Заголовок
 * @property string $description Описание
 * @property string $text Текст
 * @property string $image Изображение
 * @property int $categoryId Категория
 * @property string $createdAt Дата создания
 * @property string $updatedAt Дата изменения
 *
 * @property Category $category
 * @property Image[] $images
 */
class Article extends BaseModel
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'news';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['title', 'text'], 'required'],
			[['description', 'text', 'image'], 'string'],
			[['categoryId'], 'integer'],
			[['createdAt', 'updatedAt'], 'safe'],
			[['title'], 'string', 'max' => 128],
			[['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
			[['image'], 'string']
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'description' => 'Описание',
			'text' => 'Текст',
			'categoryId' => 'Категория',
			'createdAt' => 'Дата создания',
			'updatedAt' => 'Дата изменения',
			'image' => 'Изображение'
		];
	}

	public function beforeSave($insert)
	{
		if (!parent::beforeSave($insert)) {
			return false;
		}

		$this->uploadImage('image');

		return true;
	}

	public function uploadImage($attribute)
	{
		$file = UploadedFile::getInstance($this, $attribute);
		if ($file === null) {
			return;
		}

		$filename = ImageHelper::create($file);
		$this->$attribute = $filename;

		$file->saveAs($filename);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory()
	{
		return $this->hasOne(Category::className(), ['id' => 'categoryId']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getImages()
	{
		return $this->hasMany(Image::className(), ['newsId' => 'id']);
	}

	public function toArray(array $fields = [], array $expand = [], $recursive = true)
	{
		$data = parent::toArray($fields, $expand, $recursive);

		if (isset($data['image']) && strlen($data['image']) > 0) {
			$data['image'] = Yii::$app->urlManager->hostInfo . '/' . $data['image'];
		}

		return $data;
	}
}
